# README #

Node static Server
### What is this repository for? ###

* Quick summary
This is express static server to host Angular SPA or any other static assets.
* Version 1.0

### How do I get set up? ###
* Summary of set up

  1. Clone repo 

     git clone https://{yourUserName}@bitbucket.org/ashutec/node-spa.git

  2. Install dependency

     npm install 

* Configuration

  1. Put static content in public folder

  2. Verify configuration in environment specific configuration file 

  3. When CORS is enabled ensure correct domains are white listed 

### Contribution guidelines ###
* Code review

before pushing code ensure it is ESlinted and does not have any error

* Other guidelines

install ESlint extension in Visual studio code

### Who do I talk to? ###
* Repo owner or admin